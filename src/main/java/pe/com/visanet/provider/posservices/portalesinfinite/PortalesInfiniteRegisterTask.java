package pe.com.visanet.provider.posservices.portalesinfinite;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quiputech.ecore.v2.commons.security.SecurityCredentials;
import com.quiputech.ecore.v2.commons.tasks.Task;
import com.quiputech.ecore.v2.commons.tasks.TaskRequest;
import com.quiputech.ecore.v2.commons.tasks.TaskResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.com.visanet.provider.posservices.driver.DriverRequest;
import pe.com.visanet.provider.posservices.util.Voucher;
import pe.com.visanet.provider.posservices.util.VoucherHelper;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Michael Galdamez
 */
@Stateless
@LocalBean
public class PortalesInfiniteRegisterTask extends PortalesInfiniteTaskBase implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalesInfiniteRegisterTask.class);
    
    @PersistenceContext
    EntityManager em;
    
    @Override
    public TaskResponse executeTask(SecurityCredentials credentials, TaskRequest request) {
        TaskResponse response = new TaskResponse();
        try {
            LOGGER.info("PORTALES INFINITE");
            ObjectMapper om = new ObjectMapper();          
            //
            LOGGER.trace(om.writerWithDefaultPrettyPrinter().writeValueAsString(request));
            //
            String dateTime = String.valueOf(request.getData().get("dateTime"));
            SimpleDateFormat sdfSource = new SimpleDateFormat("yyMMddHHmmss");
            Date transactionDate = sdfSource.parse(dateTime);
            String dataRaw = String.valueOf(request.getData().get("data"));
            LOGGER.trace(dataRaw);
            HashMap<String, String> data = null;

            boolean isObject = checkForJson(dataRaw);
            String cardNumber = "";
            String cardExpirationDate = "";
            String bin = "";
            if(isObject) {

            } else {
                data = VoucherHelper.parseData(dataRaw);
                cardNumber = StringUtils.truncate(data.get("01"), SERVICE_CARD_LENGTH);
                bin = StringUtils.truncate(data.get("01"), 6);
                cardExpirationDate = StringUtils.substring(data.get("01"), 17, 21);
            }

                String merchantId = request.getMerchantId(); //String.valueOf(request.getData().get(Constants.MERCHANT_ID));
                String terminalId = request.getTerminalId(); //String.valueOf(request.getData().get(Constants.TERMINAL_ID));
                String traceNumber = request.getData().get("traceNumber"); //String.valueOf(request.getData().get("traceNumber"));

                String fechaTransaccion = sdfSource.format(transactionDate);

                DriverRequest drequest = new DriverRequest();

                drequest.setAction("registrar");

                drequest.getData().put("pan", cardNumber);
                drequest.getData().put("cardExpirationYearMonth", cardExpirationDate);
                drequest.getData().put("binNumber", bin);
                drequest.getData().put("terminalId", terminalId);
                drequest.getData().put("merchantId", merchantId);
                drequest.getData().put("traceNumber", traceNumber);
                drequest.getData().put("transactionDate", fechaTransaccion);

                LOGGER.debug(om.writerWithDefaultPrettyPrinter().writeValueAsString(drequest));
                PortalesInfiniteDriver driver = new PortalesInfiniteDriver();

                Ardef binNumber = driver.getProductId(em, bin);
                String productId = "";
                int statusCode = 400;
                String message = "";

                if (binNumber != null) {
                    productId = binNumber.getProductId().trim();
                    LOGGER.info("ProductId: " + productId);
                    statusCode = productId.equalsIgnoreCase("I")  ||
                                 productId.equalsIgnoreCase("I1") ||
                                 productId.equalsIgnoreCase("C") ||
                                 productId.equalsIgnoreCase("D")
                                 ? 0 : 1;

                    //
                    String statusAsString = statusCode == 0 ? "000" : "400"; // Estándar, no cambiar
                    //
                    String dataResponse = "";
                    switch (statusCode) {
                        case 0:
                            dataResponse = createSuccessVoucher(request, response, drequest, transactionDate, isObject);
                            response.getData().put("purchaseStatus", "00");
                            break;
                        case 1:
                            dataResponse = createFailVoucher(request, response, drequest, transactionDate, isObject);
                            response.getData().put("purchaseStatus", "50");
                            break;
                        case 2:
                            dataResponse = createReversedVoucher(request, response, transactionDate);
                            response.getData().put("purchaseStatus", "62");
                            break;
                    }

                    //
                    response.setErrorCode(statusCode);
                    response.setErrorMessage(message);
                    response.getData().put("header", "");
                    response.getData().put("data", dataResponse);
                    response.getData().put("status", statusAsString);
                    //
                    response.getData().put("serviceMessage", message);
                    response.getData().put("serviceStatus", statusAsString);
                    response.getData().put("authorizationCode", "");
                    response.getData().put("serviceRequest", "");
                    response.getData().put("serviceResponse", "");
                    response.getData().put("productName", "PORTALES - REGISTRO");

                    // LOG DB
                    response.getData().put("companyId2", "000000000000163");
                    response.getData().put("companyId3", "000000163");
                    response.getData().put("cardNumber", maskPan(cardNumber));
                    response.getData().put("cardExpirationDate", cardExpirationDate);
                    response.getData().put("closureState", "1");
                    response.getData().put("channelId", request.getData().get("channelId"));
                    response.getData().put("purchaseStatus", response.getData().get("purchaseStatus"));
                    //response.getData().put("p02", "");
                    //response.getData().put("p03", "");
                    response.getData().put("responseCode", statusAsString);
                    response.getData().put("responseDescription", message);

                }else {
                    LOGGER.trace("binNumber not found");
                    response.setErrorCode(2);
                    response.setErrorMessage("Bin no identificado");
                    String dataResponse = createFailVoucher(request, response, drequest, transactionDate, isObject);
                    response.getData().put("header", "");
                    response.getData().put("data", dataResponse);
                    response.getData().put("status", "400");             
                }
                return response;
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            String dataResponse = createInvalidParamsVoucher("REGISTRO");
            response.setErrorCode(2);
            response.setErrorMessage(e.getLocalizedMessage());
            response.getData().put("header", "");
            response.getData().put("data", dataResponse);
            response.getData().put("status", "400");
            return response;
        }
    }

    public String createFailVoucher(TaskRequest request, TaskResponse response, DriverRequest drequest, Date dateTime, boolean isObject) {
        //

        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

        // create fail voucher
        Voucher voucher = new Voucher(VOUCHER_COMPANY, "REGISTRO");
        voucher.addLine(VOUCHER_TITLE, true);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_FAILED_OPERATION, true);
        voucher.addLine(VOUCHER_COMPANY, false);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_FAILED_MSG, false);
        voucher.addLine(VOUCHER_MSG1, false);
        voucher.addLine(VOUCHER_MSG2, false);
        voucher.addEmptyLine();
        voucher.addLine("Trx: " + request.getData().get("traceNumber"), false);
        voucher.addLine("Term: " + request.getTerminalId(), false);  
        voucher.addLine("Fec:" + sdfDate.format(dateTime) + " " + sdfTime.format(dateTime), false);
        
        return voucher.getVoucher2();
    }

    public String createReversedVoucher(TaskRequest request, TaskResponse response, Date dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
        // create reversed voucher
        Voucher voucher = new Voucher(VOUCHER_COMPANY, "REGISTRO");
        voucher.addLine(VOUCHER_TITLE, true);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_FAILED_OPERATION, false);
        voucher.addLine(sdf.format(Calendar.getInstance().getTime()), false);
        voucher.addEmptyLine();
        voucher.addSmartMultiline(VOUCHER_TIMEOUT_MSG);
        voucher.addEmptyLine();
        return voucher.getVoucher2();
    }

    public String createSuccessVoucher(TaskRequest request, TaskResponse response, DriverRequest drequest, Date dateTime, boolean isObject) throws ParseException {
        //
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

        // create success voucher
        Voucher voucher = new Voucher(VOUCHER_COMPANY, "REGISTRO");
        voucher.addLine(VOUCHER_TITLE, true);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_SUB_TITLE, true);
        voucher.addLine(VOUCHER_COMPANY, false);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_SUCCESS_MSG1, false);
        voucher.addLine(VOUCHER_SUCCESS_MSG2, false);
        voucher.addLine(VOUCHER_SUCCESS_MSG3, false);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_MSG1, false);
        voucher.addLine(VOUCHER_MSG2, false);
        voucher.addLine(VOUCHER_MSG_CONDITIONS1, false);
        voucher.addLine(VOUCHER_MSG_CONDITIONS2, false);
        voucher.addEmptyLine();
        voucher.addLine("Fec:" + sdfDate.format(dateTime) + " " + sdfTime.format(dateTime), false);
        voucher.addLine("Term: " + request.getTerminalId(), false);
        voucher.addLine("Trx: " + request.getData().get("traceNumber"), false);
        
        return voucher.getVoucher2();
    }

    public String createInvalidParamsVoucher(String operation) {
        //
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
        // create fail voucher
        Voucher voucher = new Voucher(VOUCHER_COMPANY, operation);
        voucher.addLine(VOUCHER_TITLE, true);
        voucher.addEmptyLine();
        voucher.addLine(VOUCHER_FAILED_OPERATION, false);
        voucher.addLine(sdf.format(Calendar.getInstance().getTime()), false);
        voucher.addEmptyLine();
        voucher.addSmartMultiline(VOUCHER_FAILED_PARAMS_MESSAGE);
        voucher.addEmptyLine();
        return voucher.getVoucher2();
    }
    
    @Override
    public TaskResponse echo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
