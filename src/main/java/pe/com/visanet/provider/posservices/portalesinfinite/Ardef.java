package pe.com.visanet.provider.posservices.portalesinfinite;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Michael Galdamez
 */
@Entity
@Table(name = "Ardef")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ardef.findAll", query = "SELECT a FROM Ardef a"),
    @NamedQuery(name = "Ardef.findByFinalRange", query = "SELECT a FROM Ardef a WHERE a.FinalRange = :FinalRange"),
    @NamedQuery(name = "Ardef.findByInitialRange", query = "SELECT a FROM Ardef a WHERE a.InitialRange = :InitialRange"),
    @NamedQuery(name = "Ardef.findByBinNumber", query = "SELECT a FROM Ardef a WHERE a.BinNumber = :BinNumber")})
public class Ardef implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "FinalRange")
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private String FinalRange;
    @Size(max = 9)
    @Column(name = "InitialRange")
    private String InitialRange;
    @Size(max = 6)
    @Column(name = "BinNumber")
    private String BinNumber;
    @Size(max = 2)
    @Column(name = "Region")
    private String Region;
    @Size(max = 2)
    @Column(name = "Country")
    private String Country;
    @Size(max = 2)
    @Column(name = "ProductId")
    private String ProductId;
    @Size(max = 20)
    @Column(name = "Source")
    private String Source;
    @Size(max = 20)
    @Column(name = "FF")
    private String FF;

    public String getFinalRange() {
        if(FinalRange == null){
            FinalRange = "";
        }
        return FinalRange;
    }

    public void setFinalRange(String FinalRange) {
        this.FinalRange = FinalRange;
    }

    public String getInitialRange() {
        if(InitialRange == null){
            InitialRange = "";
        }
        return InitialRange;
    }

    public void setInitialRange(String InitialRange) {
        this.InitialRange = InitialRange;
    }

    public String getBinNumber() {
        if(BinNumber == null){
            BinNumber = "";
        }        
        return BinNumber;
    }

    public void setBinNumber(String BinNumber) {
        this.BinNumber = BinNumber;
    }

    public String getRegion() {
        if(Region == null){
            Region = "";
        }         
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getCountry() {
        if(Country == null){
            Country = "";
        }
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getProductId() {
        if(ProductId == null){
            ProductId = "";
        }
        return ProductId;
    }

    public void setProductId(String ProductId) {
        this.ProductId = ProductId;
    }

    public String getSource() {
        if(Source == null){
            Source = "";
        }
        return Source;
    }

    public void setSource(String Source) {
        this.Source = Source;
    }

    public String getFF() {
        if(FF == null){
            FF = "";
        }
        return FF;
    }

    public void setFF(String FF) {
        this.FF = FF;
    }

    public Ardef() {
    }
    
}
