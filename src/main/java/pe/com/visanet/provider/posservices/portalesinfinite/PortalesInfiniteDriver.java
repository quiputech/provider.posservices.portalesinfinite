package pe.com.visanet.provider.posservices.portalesinfinite;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quiputech.ecore.v2.commons.security.SecurityCredentials;
import com.quiputech.ecore.v2.commons.tasks.Task;
import com.quiputech.ecore.v2.commons.tasks.TaskRequest;
import com.quiputech.ecore.v2.commons.tasks.TaskResponse;
import com.quiputech.model.posservices.entities.TransactionLog;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import pe.com.visanet.provider.posservices.driver.Driver;
import pe.com.visanet.provider.posservices.driver.DriverRequest;
import pe.com.visanet.provider.posservices.driver.DriverResponse;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * @author Michael Galdamez
 */
public class PortalesInfiniteDriver extends PortalesInfiniteTaskBase implements Driver {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalesInfiniteDriver.class);

    @Override
    public DriverResponse execute(DriverRequest drequest) {
        ObjectMapper om = new ObjectMapper();
        DriverResponse result = new DriverResponse();
        String rawresponse = "";
        try {
            switch (drequest.getAction()) {
                case "registrar":
                break;
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage());
            result.setStatus(2);
            result.setMessage(ex.getLocalizedMessage());
        }
        return result;
    }

    public String executeRequest(String soapRequest, String URL) throws NoSuchAlgorithmException, KeyManagementException, IOException, KeyStoreException, CertificateException, UnrecoverableKeyException, ParserConfigurationException, SAXException {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpPost httppost = new HttpPost(URL);

            LOGGER.info("executing request" + httppost.getRequestLine());
            LOGGER.info("request body: " + soapRequest);
            StringEntity entity = new StringEntity(soapRequest);
            httppost.setHeader("Content-Type", "application/json");
            httppost.setHeader("Accept", "application/json");
            httppost.setEntity(entity);
            try (CloseableHttpResponse response = httpclient.execute(httppost)) {
                int statusCode = response.getStatusLine().getStatusCode();
                String statusMessage = response.getStatusLine().getReasonPhrase();
                LOGGER.info("Status: " + statusMessage);

                if (statusCode == HTTP_SUCCESS) {
                    String soapResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
                    LOGGER.info(soapResponse);
                    EntityUtils.consume(entity);
                    return soapResponse;
                } else {
                    String soapNotOk = EntityUtils.toString(response.getEntity());
                    LOGGER.error(soapNotOk);
                }
                return null;
            } catch (IOException | ParseException e) {
                LOGGER.error("Error: " + e.getLocalizedMessage());
                return null;
            }
        } catch (IOException e) {
            LOGGER.error("Error: " + e.getLocalizedMessage());
            return null;
        }

    }

    public TransactionLog getOriginalTransactionId(EntityManager em, String merchantId, String terminalId, String traceId) {
        try {
            LOGGER.trace("Looking for " + traceId);
            TypedQuery<TransactionLog> transactions = em.createNamedQuery("TransactionLog.findByMerchantIdTerminalIdTraceId", TransactionLog.class)
                    .setParameter("merchantId", merchantId)
                    .setParameter("terminalId", terminalId)
                    .setParameter("traceNumber", traceId);
            TransactionLog original = transactions.getMaxResults() > 0
                    ? transactions.getSingleResult()
                    : null;
            if (original != null) {
                LOGGER.trace("Found transaction: ", original.getTransactionData05());
                return original;
            } else {
                LOGGER.error(String.format("Unable to find transaction: m:%s-t:%s-t:%s", merchantId, terminalId, traceId));
                return null;
            }

        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            return null;
        }
    }
    
    public Ardef getProductId(EntityManager em, String binNumber) {
        try {
            LOGGER.trace("Looking for " + binNumber);
            TypedQuery<Ardef> bines = em.createNamedQuery("Ardef.findByBinNumber", Ardef.class)
                    .setParameter("BinNumber", binNumber);
            Ardef bin = bines.getMaxResults() > 0
                    ? bines.getSingleResult()
                    : null;
            if (bin != null) {
                LOGGER.trace("Found bin");
                return bin;
            } else {
                LOGGER.error(String.format("Unable to find bin: m:%s", binNumber));
                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            return null;
        }
    }
}
