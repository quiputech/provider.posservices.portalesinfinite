package pe.com.visanet.provider.posservices.portalesinfinite;

/**
 *
 * @author Michael Galdámez
 */
public class cardType {
    
       	public cardType(String tipo) {
            getCardType(tipo);
	}
        
       public static String getCardType(String id) {
        String tipo;

        switch (id) {
            case "A":
                tipo = "ATM";
                break;
            case "B":
                tipo = "business";
                break; 
            case "C":
                tipo = "clásica";
                break;
            case "D":
                tipo = "commerce";
                break;
            case "E":
                tipo = "electron";
                break;
            case "F":
                tipo = "obsoleta";
                break;
            case "G":
                tipo = "travel/money";
                break;
            case "H":
                tipo = "infinite";
                break;
            case "I":
                tipo = "reservada";
                break;
            case "J":
                tipo = "platinum/premium";
                break;
            case "K":
                tipo = "signature";
                break;
            case "L":
                tipo = "private";
                break;
            case "M":
                tipo = "Mastercard";
                break;
            case "N":
                tipo = "ATM Gateway N.";
                break;
            case "O":
                tipo = "signature business";
                break;
            case "P":
                tipo = "premier";
                break;
            case "Q":
                tipo = "propietaria";
                break;
            case "R":
                tipo = "corporativa";
                break;
            case "S":
                tipo = "purchasing";
                break;
            case "T":
                tipo = "travel voucher";
                break;
            case "V":
                tipo = "V PAY";
                break;
            case "W":
            case "X":
            case "Y":
            case "Z":    
                tipo = "reservada";
                break;
           
            default:
                tipo = "No identificada";
        }
        return tipo;
    }
}
