package pe.com.visanet.provider.posservices.portalesinfinite;

import com.quiputech.ecore.v2.commons.tasks.TaskBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author Michael Galdamez
 */
public abstract class PortalesInfiniteTaskBase extends TaskBase {

    protected static final Logger LOGGER = LoggerFactory.getLogger(PortalesInfiniteTaskBase.class);
    protected static final String VOUCHER_TITLE = "NIUBIZ";
    protected static final String VOUCHER_SUB_TITLE = "DISFRUTA DE TU BENEFICIO";
    protected static final String VOUCHER_COMPANY = "LOS PORTALES";
    protected static final String VOUCHER_MSG1 = "Exclusivo para clientes";
    protected static final String VOUCHER_MSG2 = "Visa Infinite y Signature";
    protected static final String VOUCHER_MSG_CONDITIONS1 = "Condiciones:";
    protected static final String VOUCHER_MSG_CONDITIONS2 = "vi.sa/parkingvisa";
    protected static final String VOUCHER_SUCCESS_OPERATION = "OPERACION EXITOSA";
    protected static final String VOUCHER_FAILED_OPERATION = "OPERACION RECHAZADA";
    protected static final String VOUCHER_TIMEOUT_MSG = "Lo sentimos, ha ocurrido un error al momento de establecer la conexion";
    protected static final String VOUCHER_FAILED_PARAMS_MESSAGE = "Lo sentimos, ha ocurrido un error al momento de obtener los parametros";   
    protected static final String VOUCHER_FAILED_MSG = "No cuenta con el Beneficio";
    protected static final String VOUCHER_SUCCESS_MSG1 = "Tienes 3 Horas";
    protected static final String VOUCHER_SUCCESS_MSG2 = "de estacionamiento gratis";
    protected static final String VOUCHER_SUCCESS_MSG3 = "Por consumo minimo de S/30";
    protected static final int HTTP_SUCCESS = 200;
    protected static final int SERVICE_CARD_LENGTH = 16;

    public String amount(String amount) {
        try {
            BigDecimal d = new BigDecimal(amount);
            d = d.divide(new BigDecimal(100));
            return String.format("%.2f", d.doubleValue());
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            return "";
        }
    }

    public String amount3Decimal(String amount) {
        try {
            BigDecimal d = new BigDecimal(amount);
            d = d.divide(new BigDecimal(1000));
            return String.format("%.3f", d.doubleValue());
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage());
            return "";
        }
    }

    public String formatDecimal(String value) {
        BigDecimal number = new BigDecimal(value);
        LOGGER.trace("Formatting " + value);
        DecimalFormat numberFormat = new DecimalFormat("###,###,###.00");
        numberFormat.setMinimumIntegerDigits(1);
        numberFormat.setMinimumFractionDigits(2);
        String formatted = numberFormat.format(number.doubleValue());
        LOGGER.trace("Formatted " + formatted);
        return formatted;
    }

    public String format3Decimal(String value) {
        BigDecimal number = new BigDecimal(value);
        LOGGER.trace("Formatting " + value);
        DecimalFormat numberFormat = new DecimalFormat("###,###,###.000");
        numberFormat.setMinimumIntegerDigits(1);
        numberFormat.setMinimumFractionDigits(3);
        String formatted = numberFormat.format(number.doubleValue());
        LOGGER.trace("Formatted " + formatted);
        return formatted;
    }

    public String maskPan(String pan) {
        if (pan.length() >= 16) {
            pan = pan.substring(0, 6) + "******" + pan.substring(12, 16);
        }
        return pan;
    }

    public boolean checkForJson(String mapAsString) {
        return mapAsString.contains("{");
    }
}
