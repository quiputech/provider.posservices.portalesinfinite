
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quiputech.ecore.v2.commons.tasks.TaskRequest;
import com.quiputech.ecore.v2.commons.tasks.TaskResponse;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import pe.com.visanet.provider.posservices.portalesinfinite.PortalesInfiniteTaskBase;
import pe.com.visanet.provider.posservices.portalesinfinite.PortalesInfiniteRegisterTask;

/**
 *
 * @author Michael Galdamez
 */
public class PortalesInfiniteDriverTest extends PortalesInfiniteTaskBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalesInfiniteDriverTest.class);

    public PortalesInfiniteDriverTest() {
        BasicConfigurator.configure();
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

    }

    public static String enmascarar(String nroTarjeta) {
        int len = nroTarjeta.length();
        String bin = nroTarjeta.substring(0, 6);
        String last4 = nroTarjeta.substring(len - 4);

        StringBuilder sb = new StringBuilder();
        sb.append(bin);
        for (int i = 0; i < len - 10; i++) {
            sb.append("*");
        }
        sb.append(last4);
        return sb.toString();
    }

    @Test
    public void testEnmascarar() {
        String numTarjeta = "4551478422045511";
        String masked = enmascarar(numTarjeta);
        System.out.println(masked);
    }

    @org.testng.annotations.Test(enabled = false)
    public void testDriver() throws JsonProcessingException, NoSuchAlgorithmException, KeyManagementException, IOException, KeyStoreException, CertificateException, UnrecoverableKeyException, ParserConfigurationException, SAXException, DecoderException {
        ObjectMapper om = new ObjectMapper();

        TaskRequest request = new TaskRequest();
        request.setTerminalId("24435532");
        request.setMerchantId("337564349");
        request.getData().put("traceNumber", "123456789");
        request.getData().put("data", "374078780000000055D21111201239831800000");
        request.getData().put("dateTime", "180517104412");
        LOGGER.debug(om.writerWithDefaultPrettyPrinter().writeValueAsString(request));
               
        PortalesInfiniteRegisterTask task = new PortalesInfiniteRegisterTask();
        TaskResponse response = task.executeTask(null, request);

        LOGGER.debug(om.writerWithDefaultPrettyPrinter().writeValueAsString(response));

        String hexString = String.valueOf(response.getData().get("data"));
        byte[] bytes = Hex.decodeHex(hexString.toCharArray());
        LOGGER.debug(new String(bytes, "UTF-8"));              
    }
}
